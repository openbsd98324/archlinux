# archlinux


## Content

Archlinux, the rolling distribution. 

It allows to play games, use Linux, ... some examples:

For gzdoom, pkg sdl2

and cd /opt/gzdoom

./gzdoom chocolate-doom.pk3

vega firmware support, excellent graphics (drivers/firmware),... make archlinux the favorite distribution for gamers.






## ROOTFS for easy installation

Use the command tar xvpfz on a new partition, e.g. sda3. 

https://gitlab.com/openbsd98324/archlinux2/-/raw/main/archlinux-amd64-base-1624781518.tar.gz



## LIVE Archlinux
A ready to use image for usb-stick is here:
https://gitlab.com/openbsd98324/retrogaming-ps2


